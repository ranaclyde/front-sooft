Vue.component("timeline", {
  //html
  template: `
  <div>
    <formReservation :handle-add="handleAdd" />
    <div class="card mb-3">
      <div class="card-body">
        <div ref="visualization"></div>
        <div class="menu">
          <input ref="sliderZoom" type="range" class="range" name="a" min="-1" max="1" step="0.1" v-model="slider" />
          <button @click="fit"><i class="material-icons dp48 buttons-menu">home</i></button>
          <button @click="moveLeft()"><i class="material-icons dp48 buttons-menu">arrow_back</i></button>
          <button @click="moveRight()"><i class="material-icons dp48 buttons-menu">arrow_forward</i></button>
        </div>
      </div>
    </div>
    <listItems :items="items" @delete="handleDelete" />
  </div>
  `,
  data: () => ({
    options: {
      //stack: false,
      orientation: {
        axis: "top",
        item: "top",
      },
      //zoomMax: 31536000000, // just one year
      zoomMax: 87600900, // 10,000 years is maximum possible
      zoomMin: 10000000, // 10ms
    },
    items: [
      {
        id: 1,
        content: "Reserva 1",
        start: "2020-09-10 10:00",
        end: "2020-09-10 10:30",
      },
      {
        id: 2,
        content: "Reserva 2",
        start: "2020-09-10 11:00",
        end: "2020-09-10 11:45",
      },
    ],
    slider: 0,
  }),
  computed: {
    container() {
      return this.$refs.visualization;
    },
    lastId() {
      return this.items.length ? this.items[this.items.length - 1].id : 0;
    },
  },
  mounted() {
    this.timeline = new vis.Timeline(
      this.container,
      new vis.DataSet(this.items),
      this.options
    );
  },
  watch: {
    slider(newValue) {
      console.log(newValue);
      this.sliderZoom(newValue);
    },
  },
  methods: {
    move(percentage) {
      var range = this.timeline.getWindow();
      var interval = range.end - range.start;
      this.timeline.setWindow({
        start: range.start.valueOf() - interval * percentage,
        end: range.end.valueOf() - interval * percentage,
      });
    },
    moveLeft() {
      this.move(0.2);
    },
    moveRight() {
      this.move(-0.2);
    },
    fit() {
      this.slider = 0;
      this.timeline.fit(this.items.getIds());
    },
    sliderZoom(newvalue) {
      var value = newvalue;
      if (value < 0) {
        var start = moment().year(moment().year() - 100000), // to adjust with options
          end = moment().year(moment().year() + 1);
        this.timeline.zoomOut(-value);
        if (value === -1) this.timeline.setWindow(start, end);
      } else if (value > 0) {
        var start = moment(),
          end = moment(moment().utc() + 10);
        this.timeline.zoomIn(value);
        if (value === 1) this.timeline.setWindow(start, end);
      } else {
        this.fit();
      }
      console.log(end);
    },
    // Actualiza los datos del timeline
    updateDataSet() {
      this.timeline.setItems(new vis.DataSet(this.items));
    },
    // Agrega un item al timeline
    handleAdd(newItem) {
      const item = {
        ...newItem,
        id: this.lastId + 1,
      };

      this.items.push(item);
      this.updateDataSet();
    },
    // Atrapa el evento delete y elimina el item del timeline
    handleDelete(id) {
      this.items = this.items.filter((item) => item.id != id);
      this.updateDataSet();
    }
  },
});
