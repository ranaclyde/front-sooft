Vue.component("listItems", {
  //html
  template: `
    <div class="card mb-3">
      <div class="card-header text-center">
        Listado de reservas
      </div>
      <div class="card-body">
        <table class="table mt-4">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre</th>
              <th scope="col">Horario</th>
              <th scope="col">Accion</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="item in items" :key="item.id">
              <th scope="row">{{ item.id }}</th>
              <td>{{ item.content }}</td>
              <td>de {{ item.start }} a {{ item.end }}</td>
              <td>
                <button class="btn btn-danger btn-sm" @click="removeItem(item.id)">
                  <i class="material-icons">delete</i>
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    `,
  props: {
    items: { type: Array },
  },
  methods: {
    onSubmit() {
      if (
        this.newItem.content != null &&
        this.newItem.date != null &&
        this.newItem.end != null &&
        this.newItem.start != null
      ) {
        const item = {
          content: this.newItem.content,
          start: this.newItem.date + " " + this.newItem.start,
          end: this.newItem.date + " " + this.newItem.end,
        };
        this.handleSubmit(item);
      }
    },
    removeItem(id) {
      this.$emit('delete', id);
    }
  },
});
