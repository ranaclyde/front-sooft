const EMPTY_ITEM = {
  content: null,
  date: null,
  start: null,
  end: null,
};

Vue.component("formReservation", {
  //html
  template: `
  <validation-observer v-slot="{ invalid, handleSubmit }">
    <div class="card mb-3">
      <div class="card-header text-center">
        Nueva Reserva
      </div>
      <div class="card-body">
        <form @submit.prevent="handleSubmit(onSubmit)">
          <div class="row">
            <div class="form-group col-md-4">
              <validation-provider rules="required" v-slot="{ dirty, valid, invalid, errors }">
                <label for="content">Nombre</label>
                <input class="form-control" id="content" name="Nombre" v-model="newItem.content" placeholder="Nombre">
                <div class="invalid-feedback d-inline-block" v-show="errors">{{ errors[0] }}</div>
              </validation-provider>
            </div>
            <div class="form-group col-md-2">
              <validation-provider rules="required" v-slot="{ dirty, valid, invalid, errors }">
                <label for="date">Fecha</label>
                <input type="date" class="form-control" name="Fecha" id="date" v-model="newItem.date" placeholder="Fecha">
                <div class="invalid-feedback d-inline-block" v-show="errors">{{ errors[0] }}</div>
              </validation-provider>
            </div>
            <div class="form-group col-md-2">
              <validation-provider rules="required" v-slot="{ dirty, valid, invalid, errors }">
                <label for="start">Hora inicio</label>
                <input type="time" class="form-control" name="Fecha inicio" id="start" v-model="newItem.start" placeholder="Hora inicio">
                <div class="invalid-feedback d-inline-block" v-show="errors">{{ errors[0] }}</div>
              </validation-provider>
            </div>
            <div class="form-group col-md-2">
              <validation-provider rules="required" v-slot="{ dirty, valid, invalid, errors }">
                <label for="end">Hora fin</label>
                <input type="time" class="form-control" name="Fecha fin" id="end" v-model="newItem.end" placeholder="Hora fin">
                <div class="invalid-feedback d-inline-block" v-show="errors">{{ errors[0] }}</div>
              </validation-provider>
            </div>
            <div class="form-group col-md-2">
              <button class="btn btn-success mt-4" :disabled="invalid">Reservar</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </validation-observer>
    `,
  props: {
    handleAdd: { type: Function },
  },
  data: () => ({
    newItem: { ...EMPTY_ITEM },
  }),
  methods: {
    onSubmit() {
      const item = {
        content: this.newItem.content,
        start: this.newItem.date + " " + this.newItem.start,
        end: this.newItem.date + " " + this.newItem.end,
      };
      this.handleAdd(item);
      this.newItem = { ...EMPTY_ITEM }
    },
  },
});
